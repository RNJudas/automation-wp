This project deals with the automation for deployment of wordpress websites on the old stack.

It creates the following setups.
-- Wordpress Project Core Setup.
-- MySQL for Wordpress websites.
-- Nginx on Wordpress Upstreams.
-- MUPlugins.
-- Git Repo for Wordpress project.
-- Git remote setup.
-- User configuration.
-- DUO account and applicaiton setup.
-- Statuscake Monitoring.
-- Unit testing of deployment.

To run a deployment from the CLI the following details need to be provided.

#Usage: ansible-playbook -i localhost play.yml -e "ROLE=wp-dev-deploy" -e "TARGETENV=localhost" -e "USER=ubuntu" -e "AS_TITLE=Automation" -e "AS_URL=www-autoinfra-dev.kurtosysweb.com" -e "AS_NUM=9999"  -e "AS_ENV=dev" -vv

Please report all errors to: renaldo.maclons@kurtosys.com

This Project is still in development.
